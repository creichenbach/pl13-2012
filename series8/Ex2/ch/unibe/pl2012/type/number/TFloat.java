package ch.unibe.pl2012.type.number;

public class TFloat {

	private float n;

	public TFloat(float n) {
		this.n = n;
	}

	public TFloat times(TInteger int1) {
		return new TFloat(n * int1.unwrap());
	}

	public TFloat times(TFloat float1) {
		return new TFloat(n * float1.unwrap());
	}

	public float unwrap() {
		return n;
	}

}
