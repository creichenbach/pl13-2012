package ch.unibe.pl2012.type.number.test;


import ch.unibe.pl2012.type.number.TFloat;
import ch.unibe.pl2012.type.number.TInteger;

public class TNumberTest {

	public final static float DELTA = 0.000001f;
	
	public TInteger getInt(int n) {
		return new TInteger(n);
	}

	public TFloat getFloat(float n) {
		return new TFloat(n);
	}
}
