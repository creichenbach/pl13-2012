package ch.unibe.pl2012.type.number;

public class TInteger {

	private int n;

	public TInteger(int n) {
		this.n = n;
	}

	public TInteger times(TInteger int1) {
		return new TInteger(n * int1.unwrap());
	}

	public TFloat times(TFloat float1) {
		return new TFloat(n * float1.unwrap());
	}

	public int unwrap() {
		return n;
	}

}
