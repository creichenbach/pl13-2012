:- ensure_loaded(library(operators)). % load readable operators
:- op(900, xfy, to). % define new infix operator �to�

hanoi(1,A,B,_,M) :- M = [A to B], !. % red cut to prevent infinite loop (unique solution)
hanoi(N,A,B,C,M) :-
	X is N-1,
	hanoi(X, A, C, B, M2),
	append(M2, [A to B], MTemp),
	hanoi(X, C, B, A, M3),
	append(MTemp, M3, M).