% ----- general helpers -----

not(X) :- X, !, fail.
not(_).

printAll(X) :- X, print(X), nl, fail.
printAll(_).

% ----- specific rules -----

notInline(A, B) :- A \= B.

diag(A, B, D) :- B is A + D.
diag(A, B, D) :- A is B + D.

notDiagonal(A, B, D) :- not(diag(A, B, D)).

% Check X with all elements in list. D: distance between X and H
noConflict(_, [], _) :- !.
noConflict(X, [H|L], D) :- notInline(X, H), notDiagonal(X, H, D), noConflict(X, L, D+1).

safePosition([_|[]]) :- !.
safePosition([X|L]) :- noConflict(X, L, 1), safePosition(L).

size([],S) :- S is 0.
size([_|L], S) :- size(L,SL), S is SL+1.

inList(X, [H|_]) :- X is H.
inList(X, [_|L]) :- inList(X, L).

permutations(L, P, R) :- size(L,S1), size(P,S2), S1 = S2, R = P, !.
permutations(L, P, R) :- inList(X, L), not(inList(X, P)),
	append([X],P,P2),  permutations(L, P2, R).

nQueens(L) :- permutations(L,[],R), safePosition(R), print(R), nl, fail.
nQueens(_).

makeList(0, L) :- L = [ ], !.
makeList(N, L) :- M is (N-1), makeList(M,L2), append(L2, [N], L).

nQueenSide(N) :- makeList(N,L), nQueens(L).