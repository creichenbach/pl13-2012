% Facts (just some examples from the lecture)
:- dynamic parent/2, male/1, female/1.

female(anne). parent(andrew, elizabeth).
female(diana). parent(andrew, philip).
female(elizabeth). parent(anne, elizabeth).
parent(anne, philip).
male(andrew). parent(charles, elizabeth).
male(charles). parent(charles, philip).
male(edward). parent(edward, elizabeth).
male(harry). parent(edward, philip).
male(philip). parent(harry, charles).
male(william). parent(harry, diana).
parent(william, charles).
parent(william, diana).

parent(oskar,mario).
parent(oskar,monika).
parent(jon,adrian).
parent(jon,anna).
parent(adrian, hermann).
parent(monika, hermann).
parent(adrian, rosi).
parent(monika, rosi).
male(hermann).
female(rosi).

% Rules (global)

not(X) :- X, !, fail.
not(_).

printall(X) :- X, print(X), nl, fail.
printall(_).

% Rules (considered)

sibling(A,B) :- father(A, F), father(B, F), 
			mother(A, M), mother(B, M),
			not(A=B).
brother(A,B) :- sibling(A,B), male(B).
sister(A,S) :- sibling(A,S), female(S).

father(C, F) :- parent(C,F), male(F).
mother(C, F) :- parent(C,F), female(F).

aunt(X,A) :- parent(X,P), sibling(P,A), female(A).
uncle(X,U) :- parent(X,P), sibling(P,U), male(U).

child(P,C) :- parent(C,P).
son(P,S) :- parent(S,P), male(S).
daughter(P,D) :- parent(D,P), female(D).

% Rules (determined)

grandparent(C, G) :- parent(C,P), parent(P,G).
grandfather(C, G) :- grandparent(C,G), male(G).
grandmother(C, G) :- grandparent(C,G), female(G).

grandchild(G,C) :- grandparent(C,G).
grandson(G,S) :- grandparent(S,G), male(S).
granddaughter(G,D) :- grandparent(D,G), female(D).

niece(X, N) :- sibling(X,Y), daughter(Y,N).
nephew(X, N) :- sibling(X,Y), son(Y,N).
cousin(C,D) :- parent(C,X), parent(D,Y), sibling(X,Y).