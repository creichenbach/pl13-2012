% Facts

line(a,b).
line(b,d).
line(c,a).
line(c,d).
line(d,e).
line(c,e).
line(f,h).
line(h,i).
line(i,j).
line(f,g).
line(g,j).

% Rules (global)

not(X) :- X, !, fail.
not(_).

printall(X) :- X, print(X), nl, fail.
printall(_).

% Rules (exercise)

connected(A,B) :- line(A,B).
connected(A,B) :- line(B,A).

triangle(A,B,C) :- connected(A,B), connected(B,C), connected(C,A).

quadrangle(A,B,C,D) :- connected(A,B), connected(B,C), connected(C,D), connected(D,A), not(A=C), not(B=D).

reachable(A,B) :- line(A,B).
reachable(A,B) :- line(A,X), reachable(X,B).