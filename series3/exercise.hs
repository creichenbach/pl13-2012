-- ~Exercise 2~

-- triangle
triangle 1 = 1
triangle n = n + triangle(n-1)

triangle_tr n = tri_tr_helper n 0
tri_tr_helper x s =	if		x == 1
					then	s + 1
					else	tri_tr_helper (x-1) (x+s)

triangle_equal_until x = (map triangle [1..x] == map triangle_tr [1..x])
	
-- tetrahedral	
tetrahedral 1 = 1
tetrahedral n = triangle n + tetrahedral (n - 1)

tetrahedral_tr n = te_tr_helper n 0
te_tr_helper x s =	if		x == 1
					then	s + 1
					else	te_tr_helper (x-1) (triangle(x) + s)

tetrahedral_equal_until x = (map tetrahedral [1..x] == map tetrahedral_tr [1..x])

-- ~Exercise 3~

-- just for understanding foldr
minus x y = x - y

-- factorial function
fac x = foldr (*) 1 [1..x]

-- ~Exercise 4~

-- a)
printNodes [x] = show x
printNodes (x:xs) = (show x) ++ (printNodes xs)

-- b)
insertNode n [] = [n]
insertNode n (x:xs) = 	if		x > n
						then	n:x:xs
						else	x:(insertNode n xs)
						
-- c)
deleteNodes p [] = []
deleteNodes p (x:xs) =	if		(p x)
						then	deleteNodes p xs
						else	x:(deleteNodes p xs)

						